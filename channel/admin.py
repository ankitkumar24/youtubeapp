from django.contrib import admin

from channel.models import Channel
from import_export.admin import ImportExportModelAdmin


class ImportExport(ImportExportModelAdmin):
    list_display=['channel_name','status','user']


admin.site.register(Channel,ImportExport)
