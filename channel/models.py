
from django.db import models
from taggit.managers import TaggableManager
from django.conf import settings



User = settings.AUTH_USER_MODEL
VISIBILITY_CHOICES = [
    ("active", "Active"),
    ("disable", "Disable"),
]

# def user_directory_path(instance, filename):
#     username = instance.user.username
#     ext = filename.split('.')[-1]
#     file_path = f"media/{username}/{filename}"
#     return file_path

def user_directory_path(instance, filename):
    return "user_{0}/{1}".format(instance.user.id, filename)

class Channel(models.Model):
    image = models.ImageField(upload_to='static/media/images', null=True, blank=True)
    full_name = models.CharField(max_length=100)
    channel_name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    keywords = TaggableManager()
    joined = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=VISIBILITY_CHOICES, max_length=100, default="public")
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='channel')
    subscribers = models.ManyToManyField(User, related_name='user_subs')
    verified = models.BooleanField(default=False)
    
    def __str__(self):
        return self.channel_name
