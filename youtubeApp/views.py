from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from youtubeApp.models import Video, Comment
from youtubeApp.models import Video
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from youtubeApp.forms import VideoForm
# from django.conf import settings


# User = settings.AUTH_USER_MODEL
# Create your views here.
def index(request):
    video = Video.objects.filter(visibility='public')
    context = {
        "video":video
    }
    return render(request,'youtubeApp/index.html',context)
def videoDetail(request, pk):
    video = Video.objects.get(id=pk)
    videos = Video.objects.exclude(id=pk).filter(visibility='public')
    video.views = video.views + 1
    video.save()
    comment = Comment.objects.filter(active=True, video=video).order_by('-date')
    context={
        'video':video,
        'comment':comment,
        'videos':videos
    }
    return render(request,'youtubeApp/video.html',context)

# @login_required()
def video_upload(request):
    if request.method == 'POST':
        form = VideoForm(request.POST, request.FILES)
        if form.is_valid():
            # Save the form data
            video = form.save(commit=False)
            # video.user = request.user  # Assuming you have authentication implemented
            video.save()
            # Redirect or show success message
            return redirect('index')
    else:
        form = VideoForm()
    return render(request, 'youtubeApp/upload-video.html', {'form': form})