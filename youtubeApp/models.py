from django.db import models
from taggit.managers import TaggableManager
from django.conf import settings


User = settings.AUTH_USER_MODEL
VISIBILITY_CHOICES = [
    ("public", "Public"),
    ("private", "Private"),
    ("unlisted", "Unlisted"),
]

# def user_directory_path(instance, filename):
#     username = instance.user.username
#     ext = filename.split('.')[-1]
#     file_path = f"media/{username}/{filename}"
#     return file_path

def user_directory_path(instance, filename):
    return "user_{0}/{1}".format(instance.user.id, filename)

class Video(models.Model):
    video = models.FileField(upload_to='static/media/video')
    image = models.ImageField(upload_to='static/media/images', null=True, blank=True)
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    tags = TaggableManager()
    date = models.DateTimeField(auto_now_add=True)
    visibility = models.CharField(choices=VISIBILITY_CHOICES, max_length=100, default="public")
    # user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    views = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title

class Comment(models.Model):
    comment = models.CharField(max_length=10000)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    active = models.BooleanField(default=True)
    video = models.ForeignKey(Video, on_delete=models.CASCADE, related_name="comments")
    date = models.DateTimeField(auto_now_add=True)

    def __ste__(self):
        return self.comment[:30]
