
from django.urls import path,include
from . import views

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.index, name='index'),
    path('video/<int:pk>/',views.videoDetail,name='video'),
    path('upload-video/', views.video_upload, name='upload'),
    
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)