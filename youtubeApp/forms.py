from django import forms
from youtubeApp.models import Video

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['video', 'image', 'title', 'description', 'tags', 'visibility']
        widgets = {
            'video': forms.FileInput(attrs={'class': 'file'}),
            'image': forms.FileInput(attrs={'class': 'file'}),
        }
        labels = {
            'video': 'Video File',
            'image': 'Image File',
        }
