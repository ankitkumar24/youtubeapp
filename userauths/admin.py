from django.contrib import admin
from userauths.models import User
# Register your models here.
from import_export.admin import ImportExportModelAdmin


# Register your models here.
class ImportExport(ImportExportModelAdmin):
    pass

admin.site.register(User,ImportExport)