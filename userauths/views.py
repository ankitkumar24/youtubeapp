from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib import messages
from userauths.forms import UserRegisterForm
from django.contrib.auth import authenticate, login 
from django.contrib.auth import logout

User = settings.AUTH_USER_MODEL


from django.contrib.auth import get_user_model

def RegisterView(request):
    if request.user.is_authenticated:
        return redirect("youtubeApp:index")

    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            username = form.cleaned_data.get("username")
            messages.success(request, f"Hey {username}, Account Created.")

            # Authenticate the new user
            new_user = authenticate(username=new_user.email, password=form.cleaned_data['password'])
            if new_user is not None:
                login(request, new_user)
                return redirect('sign-in')
            else:
                messages.warning(request, "Failed to authenticate the user.")

    else:
        form = UserRegisterForm()

    context = {
        "form": form,
    }
    return render(request, "userauths/sign-up.html", context)




from django.contrib.auth.forms import AuthenticationForm

def loginView(request):
    if request.user.is_authenticated:
        return redirect("youtubeApp:index")

    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            email = form.cleaned_data.get("username")  # Retrieve the entered email
            password = form.cleaned_data.get("password")  # Retrieve the entered password

            user = authenticate(request, email=email, password=password)

            if user is not None:
                login(request, user)
                messages.success(request, "You are logged in")
                return redirect("youtubeApp:index")
            else:
                messages.warning(request, "Invalid email or password")
        else:
            messages.warning(request, "Invalid email or password")
    else:
        form = AuthenticationForm()

    context = {
        "form": form,
    }
    return render(request, "userauths/sign-in.html", context)



def logoutView(request):
    logout(request)
    return redirect("sign-in")

