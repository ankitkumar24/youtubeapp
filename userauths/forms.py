from django import forms
from django.contrib.auth.forms import UserCreationForm
from userauths.models import User 

class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 
            'email', 
            'first_name', 
            'last_name', 
            'password' 
            ]
        widgets = {
            'password': forms.PasswordInput(attrs={'class':'', 'id':'reg_pass_word', 'onkeyup': 'validate()'}),
        }