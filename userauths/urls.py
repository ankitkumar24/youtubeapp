import imp
from django.urls import path,include
from userauths import *
from youtubeApp import urls as url, views as view
from userauths.views import RegisterView, loginView, logoutView
from django.conf import settings
from django.conf.urls.static import static





urlpatterns = [
    path("sign-up/", RegisterView, name="sign-up"),
    path("sign-in/", loginView, name="sign-in"),
    path("sign-out/", logoutView, name="sign-out"),
    # path('',view.index, name='index'),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + url.urlpatterns